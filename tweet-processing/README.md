# Tweet Processing

A set of Spark applications required to run tweet processing.

Before building install [sbt-assembly plugin](https://github.com/sbt/sbt-assembly)

To build, run : `sbt assembly`