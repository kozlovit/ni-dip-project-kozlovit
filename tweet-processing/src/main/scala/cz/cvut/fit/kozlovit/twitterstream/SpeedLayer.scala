package cz.cvut.fit.kozlovit.twitterstream

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.ml.feature.RegexTokenizer
import org.apache.spark.sql.{Column, DataFrame, SparkSession}
import org.apache.spark.sql.avro.functions.{from_avro, to_avro}
import org.apache.spark.sql.cassandra.DataFrameWriterWrapper
import org.apache.spark.sql.functions.{col, explode, floor, hour, minute, struct, typedLit}
import org.apache.spark.sql.types.{DateType, IntegerType, TimestampType}
import org.apache.spark.sql.catalyst.expressions.objects.AssertNotNull

import scala.io.Source

/** Speed processing layer of the system
 *
 *  ==Overview==
 *  The main class to run data processing of tweet stream and the source of data for speed views
 *  It operates on a stream from Kafka source. The processed records are saved in Cassandra
 *  and streamed to Kafka as a source for other services.
 *
 */
object SpeedLayer {

  val conf: Config = ConfigFactory.load()
  val settings: Settings = new Settings(conf)

  def main(args: Array[String]): Unit = {
    val spark = getSparkSession
    val tweetStream = getTweetStream(spark)
    val processedRecords = processStream(tweetStream)
    streamResults(processedRecords)
    spark.stop()
  }

  /** Creates and returns Spark Session */
  private def getSparkSession: SparkSession = {
    SparkSession
      .builder
      .appName("SpeedLayer")
      .config("spark.sql.session.timeZone", "UTC")
      .config("spark.cassandra.connection.host", settings.cassandra("host"))
      .config("spark.cassandra.auth.username", settings.cassandra("username"))
      .config("spark.cassandra.auth.password", settings.cassandra("password"))
      .config("spark.sql.shuffle.partitions", settings.spark_shuffle_partitions("speed_layer"))
      .getOrCreate()
  }

  /** Retrieves tweet stream from Kafka and decodes records for further processing */
  private def getTweetStream(spark: SparkSession): DataFrame = {
    val tweetSchema: String = Source.fromInputStream(
      getClass.getResourceAsStream(settings.schemas("tweet"))).mkString

    val stream = spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", settings.kafka("address_consumer"))
      .option("subscribe", settings.kafka("topic_tweets"))
      .option("minPartitions", settings.kafka_min_partitions("speed_layer"))
      .option("maxOffsetsPerTrigger", settings.max_offsets_per_trigger("speed_layer"))
      .load()

    stream
      .select(from_avro(col("value"), tweetSchema).as("tweet"))
      .withColumn("ts_seconds", (col("tweet.timestamp") / 1000).cast(TimestampType))
      .select("tweet.tweet", "tweet.timestamp", "tweet.tweetid", "ts_seconds")
  }

  /**
   * Processes the stream.
   *
   * Retrieves hashtags (words starting with # symbol) from tweets and creates date/time columns
   * columns to match Cassandra table schema.
   *
   */
  private def processStream(tweetStream: DataFrame): DataFrame = {
    val hashtags = new RegexTokenizer()
      .setGaps(false)
      .setPattern("#(\\w+)")
      .setMinTokenLength(2)
      .setInputCol("tweet")
      .setOutputCol("hashtags")
      .transform(tweetStream)
      .withColumn("hashtag", explode(col("hashtags")))

    hashtags
      .withColumn("tweetid", new Column(AssertNotNull(col("tweetid").expr)))
      .withColumn("timestamp", new Column(AssertNotNull(col("timestamp").expr)))
      .withColumn("hashtag", new Column(AssertNotNull(col("hashtag").expr)))
      .withColumn("date", col("ts_seconds").cast(DateType))
      .withColumn("hour", hour(col("ts_seconds")))
      .withColumn("minute", minute(col("ts_seconds")))
      .withColumn("min_5", (floor(minute(col("ts_seconds")) / 5) * 5).cast(IntegerType))
      .withColumn("occurrences", typedLit(1))
  }

  /**
   * Write results to Cassandra and Kafka sinks.
   *
   * The micro-batches are cached for better performance and then saved in both Cassandra
   * speed view tables.
   *
   */
  private def emit(batch:DataFrame, batchID:Long): Unit = {
    batch.persist()

    //store records to Cassandra speed views
    batch
      .select("hashtag", "date", "hour", "min_5", "occurrences")
      .write
      .cassandraFormat(settings.cassandra("speed_view_0"), settings.cassandra("keyspace"))
      .mode("append")
      .save

    batch
      .select("hashtag", "date", "hour", "min_5", "occurrences")
      .write
      .cassandraFormat(settings.cassandra("speed_view_1"), settings.cassandra("keyspace"))
      .mode("append")
      .save

    //store records to Kafka
    batch
      .select(to_avro(struct(col("hashtag"), col("tweetid"), col("timestamp"))) as "value")
      .write
      .format("kafka")
      .option("kafka.bootstrap.servers", settings.kafka("address_consumer"))
      .option("topic", settings.kafka("topic_hashtags"))
      .save()

    batch.unpersist()
  }

  /** Starts streaming data to multiple sinks */
  private def streamResults(processedRecords: DataFrame): Unit = {
    val hashtagSchema: String = Source.fromInputStream(
      getClass.getResourceAsStream(settings.schemas("hashtag"))).mkString

    val query = processedRecords
      .writeStream
      .foreachBatch(emit _)
      .outputMode("append")
      .option("checkpointLocation", settings.checkpointPaths("speed_layer"))
      .start()

    query.awaitTermination()
  }
}
