package cz.cvut.fit.kozlovit.twitterstream

import com.datastax.driver.core.Cluster
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.ml.feature.RegexTokenizer
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.avro.functions.from_avro
import org.apache.spark.sql.cassandra.DataFrameWriterWrapper
import org.apache.spark.sql.functions.{col, explode, floor, hour, minute, typedLit}
import org.apache.spark.sql.streaming.Trigger
import org.apache.spark.sql.types.{DateType, TimestampType}

import scala.io.Source

/** Batch processing layer of the system
 *
 *  ==Overview==
 *  The main class to run batch pipeline, which:
 *    1) Enriches the master dataset by the newly accumulated data from Kafka source
 *    since the last run of the pipeline.
 *    2) Processes the master dataset and updates relevant tables (batch view) in Cassandra
 *    with the processing results.
 *    3) Participates on the synchronization of batch and speed layers by truncating redundant data
 *    and signaling which tables should be queried through Zookeeper.
 *
 */
object BatchLayer {

  val conf: Config = ConfigFactory.load()
  val settings: Settings = new Settings(conf)

  def main(args: Array[String]): Unit = {
    val spark = getSparkSession

    truncateInactiveSpeedView()

    persistNewTweets(spark)

    val processedRecords = processTweets(spark)
    storeProcessedRecords(processedRecords)
    spark.stop()

    SpeedViewManager.toggleView()
    truncateInactiveBatchView()
  }

  /** Creates and returns Spark Session */
  private def getSparkSession: SparkSession = {
    SparkSession
      .builder
      .appName("BatchLayer")
      .config("spark.sql.session.timeZone", "UTC")
      .config("spark.cassandra.connection.host", settings.cassandra("host"))
      .config("spark.cassandra.auth.username", settings.cassandra("username"))
      .config("spark.cassandra.auth.password", settings.cassandra("password"))
      .getOrCreate()
  }

  /**
   * Retrieves new tweets from Kafka and appends them to master dataset.
   *
   * Only retrieving tweets created after the last checkpoint (last batch job run).
   * The data is partitioned by date and hour and stored parquet format.
   */
  private def persistNewTweets(spark: SparkSession): Unit = {
    val tweetSchema: String = Source.fromInputStream(
      getClass.getResourceAsStream(settings.schemas("tweet"))).mkString

    val newTweets = spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", settings.kafka("address_producer"))
      .option("subscribe", settings.kafka("topic_tweets"))
      .load()
      .select(from_avro(col("value"), tweetSchema).as("tweet"))
      .select("tweet.*")

    val query = newTweets
      .withColumn("date", (col("timestamp") / 1000).cast(TimestampType).cast(DateType))
      .withColumn("hour", hour((col("timestamp") / 1000).cast(TimestampType)))
      .writeStream
      .partitionBy("date", "hour")
      .option("checkpointLocation", settings.checkpointPaths("master_dataset_supplier"))
      .option("path", settings.dataPaths("master_dataset"))
      .trigger(Trigger.Once())
      .start()

    query.awaitTermination()
  }

  /** Processes the master dataset. */
  private def processTweets(spark: SparkSession): DataFrame = {
    val tweets = spark
      .read
      .parquet(settings.dataPaths("master_dataset"))
      .select("tweet", "timestamp", "date", "hour")

    val hashtags = new RegexTokenizer()
      .setGaps(false)
      .setPattern("#(\\w+)")
      .setMinTokenLength(2)
      .setInputCol("tweet")
      .setOutputCol("hashtags")
      .transform(tweets)
      .withColumn("hashtag", explode(col("hashtags")))

    hashtags
      .withColumn("minute", minute((col("timestamp") / 1000).cast(TimestampType)))
      .withColumn("min_5", floor(col("minute") / 5) * 5)
      .withColumn("occurrences", typedLit(1))
      .groupBy("date", "hour", "min_5", "hashtag")
      .count()
      .withColumnRenamed("count", "occurrences")
      .select("hashtag", "date", "hour", "min_5", "occurrences")
  }

  /** Store records to Cassandra inactive batch view */
  def storeProcessedRecords(processedRecords: DataFrame): Unit = {
    val inactiveView = if (SpeedViewManager.getActiveView == "0") "1" else "0"

    processedRecords
      .write
      .cassandraFormat(
        settings.cassandra(s"batch_view_$inactiveView"),
        settings.cassandra("keyspace"))
      .mode("append")
      .save
  }

  private def truncateInactiveView(layer: String): Unit = {
    val keyspace = settings.cassandra("keyspace")
    val inactiveView = if (SpeedViewManager.getActiveView == "0") "1" else "0"
    val tableToTruncate = settings.cassandra(s"${layer}_view_$inactiveView")
    val query = s"TRUNCATE $keyspace.$tableToTruncate;"

    val cluster = Cluster
      .builder
      .withoutJMXReporting
      .addContactPoints(settings.cassandra("host"))
      .withCredentials(
        settings.cassandra("username"),
        settings.cassandra("password"))
      .build

    val session = cluster.newSession()
    session.execute(query)
    session.close()
    cluster.close()
  }

  /**
   * Truncates one of the tables in Speed view to resolve redundant data.
   *
   * After the speed view table is truncated, it's data will not overlap with the batch view and will provide
   * complete history data when activated and combined with batch view after the current job is over.
   *
   */
  private def truncateInactiveSpeedView(): Unit = {
    truncateInactiveView("speed")
  }

  /** Truncates old batch view */
  private def truncateInactiveBatchView(): Unit = {
    truncateInactiveView("batch")
  }
}
