package cz.cvut.fit.kozlovit.twitterstream

import com.typesafe.config.Config

class Settings(config: Config) extends Serializable {

  var schemas: Map[String, String] = {
    Map(
      "tweet" ->  config.getString("schemas.tweet"),
      "hashtag" -> config.getString("schemas.hashtag")
    )
  }

  var cassandra: Map[String, String] = {
    Map(
      "host" -> config.getString("cassandra.host"),
      "keyspace" -> config.getString("cassandra.keyspace"),
      "username" -> config.getString("cassandra.username"),
      "password" -> config.getString("cassandra.password"),
      "batch_view_0" -> config.getString("cassandra.tables.batch_view_0"),
      "batch_view_1" -> config.getString("cassandra.tables.batch_view_1"),
      "speed_view_0" -> config.getString("cassandra.tables.speed_view_0"),
      "speed_view_1" -> config.getString("cassandra.tables.speed_view_1")
    )
  }

  var kafka: Map[String, String] = {
    Map(
      "address_producer" -> config.getString("kafka.address_producer"),
      "address_consumer" -> config.getString("kafka.address_consumer"),
      "topic_tweets" -> config.getString("kafka.topics.tweets"),
      "topic_hashtags" -> config.getString("kafka.topics.hashtags"),
      "topic_trends" -> config.getString("kafka.topics.trends")
    )
  }

  var spark_shuffle_partitions: Map[String, Int] = {
    Map(
      "speed_layer" -> config.getString("spark.shuffle_partitions.speed_layer").toInt,
      "hashtag_trends" -> config.getString("spark.shuffle_partitions.hashtag_trends").toInt
    )
  }

  var kafka_min_partitions: Map[String, Int] = {
    Map(
      "speed_layer" -> config.getString("kafka.min_partitions.speed_layer").toInt,
      "hashtag_trends" -> config.getString("kafka.min_partitions.hashtag_trends").toInt
    )
  }

  var max_offsets_per_trigger: Map[String, Int] = {
    Map(
      "speed_layer" -> config.getString("spark.max_offsets_per_trigger.speed_layer").toInt,
      "hashtag_trends" -> config.getString("spark.max_offsets_per_trigger.hashtag_trends").toInt
    )
  }

  var dataPaths: Map[String, String] = {
    Map(
      "master_dataset" -> config.getString("paths.data.master_dataset")
    )
  }

  var checkpointPaths: Map[String, String] = {
    Map(
      "master_dataset_supplier" -> config.getString("paths.checkpoints.master_dataset_supplier"),
      "speed_layer" -> config.getString("paths.checkpoints.speed_layer"),
      "hashtag_trends" -> config.getString("paths.checkpoints.hashtag_trends")
    )
  }

  var zookeeper: Map[String, String] = {
    Map(
      "address"-> config.getString("zookeeper.address"),
      "active_view_path"-> config.getString("zookeeper.active_view_path")
    )
  }

}