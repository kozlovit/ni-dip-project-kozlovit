package cz.cvut.fit.kozlovit.twitterstream

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.avro.functions.{from_avro, to_avro}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{col, collect_list, max, struct, unix_timestamp, window}
import org.apache.spark.sql.types.{ArrayType, LongType, StringType, StructField, StructType, TimestampType}

import scala.io.Source

/** Processing of hashtag stream and results aggregation.
 *
 *  ==Overview==
 *  The main class to produce trending hashtag list based on Kafka hashtag stream.
 *  The stream is processed and aggregated over sliding event windows and
 *  the list of hashtags with the most occurrences in the last n minutes is produced.
 *
 */
object HashtagTrends {

  val conf: Config = ConfigFactory.load()
  val settings: Settings = new Settings(conf)

  def main(args: Array[String]): Unit = {
    val spark = getSparkSession
    val hashtagStream = getHashtagStream(spark)
    val processedRecords = processStream(hashtagStream)
    streamResults(processedRecords)
    spark.stop()
  }

  /** Creates and returns Spark Session */
  private def getSparkSession: SparkSession = {
    SparkSession
      .builder
      .appName("HashtagTrends")
      .config("spark.sql.session.timeZone", "UTC")
      .config("spark.cassandra.connection.host", settings.cassandra("host"))
      .config("spark.cassandra.auth.username", settings.cassandra("username"))
      .config("spark.cassandra.auth.password", settings.cassandra("password"))
      .config("spark.sql.shuffle.partitions", settings.spark_shuffle_partitions("hashtag_trends"))
      .config("spark.sql.debug.maxToStringFields", 1000)
      .config("spark.sql.codegen.wholeStage", "false")
      .getOrCreate()
  }

  /** Retrieves hashtag stream from Kafka and decodes records for further processing */
  private def getHashtagStream(spark: SparkSession): DataFrame = {
    val stream = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", settings.kafka("address_producer"))
      .option("subscribe", settings.kafka("topic_hashtags"))
      .option("minPartitions", settings.kafka_min_partitions("hashtag_trends"))
      .option("maxOffsetsPerTrigger", settings.max_offsets_per_trigger("hashtag_trends"))
      .load()

    val hashtagSchema: String = Source.fromInputStream(
      getClass.getResourceAsStream(settings.schemas("hashtag"))).mkString

    stream
      .select(from_avro(col("value"), hashtagSchema).as("hashtag"))
      .withColumn("ts_seconds", (col("hashtag.timestamp") / 1000).cast(TimestampType))
      .select("hashtag.hashtag", "ts_seconds")
  }

  /** Calculates hashtag occurrences over sliding event time window */
  private def processStream(hashtagStream: DataFrame): DataFrame = {
    hashtagStream
      .withWatermark("ts_seconds", "20 seconds")
      .groupBy(
        window(col("ts_seconds"), "10 minutes", "10 seconds").as("time_window"),
        col("hashtag")
      )
      .count()
  }

  /**
   *
   * Retrieves hashtags with the most occurrences during the finished time window.
   * Be aware that more that one time window can be present in the micro-batch in case of computation delay.
   * In that case only the latest finished time window is processed.
   *
   */
  private def getTrendRecords(spark: SparkSession, hashtagBatch: DataFrame): DataFrame = {
    val topHashtags = hashtagBatch
      .withColumnRenamed("count", "occurrences")
      .orderBy(col("time_window.end").desc, col("occurrences").desc)
      .limit(10)
      .withColumn("time_bucket",max(col("time_window.end")).over(Window.partitionBy()))
      .filter(col("time_bucket") === col("time_window.end"))

    val trendsRecords = topHashtags
      .withColumn("timestamp", unix_timestamp(col("time_bucket")))
      .withColumn("hashtag_record", struct(col("hashtag"), col("occurrences")))
      .groupBy("timestamp")
      .agg(collect_list(col("hashtag_record")).as("hashtag_list"))
      .select("timestamp", "hashtag_list")

    val trendsSchema: StructType = StructType(List(
      StructField("timestamp", LongType, nullable = false),
      StructField("hashtag_list", ArrayType(
        StructType(List(
          StructField("hashtag", StringType, nullable = false),
          StructField("occurrences", LongType, nullable = false)
        )), containsNull = false
      ), nullable = false)
    ))

    spark.createDataFrame(trendsRecords.rdd, schema = trendsSchema)
  }

  private def emit(batch:DataFrame, batchID:Long): Unit = {
    val trendRecords = getTrendRecords(getSparkSession, batch)

    trendRecords
      .select(to_avro(struct("timestamp", "hashtag_list")) as "value")
      .write
      .format("kafka")
      .option("kafka.bootstrap.servers", settings.kafka("address_consumer"))
      .option("topic", settings.kafka("topic_trends"))
      .save()
  }

  /** Streams records in avro format to the dedicated topic in Kafka.  */
  private def streamResults(processedRecords: DataFrame): Unit = {
    val query = processedRecords.writeStream
      .foreachBatch(emit _)
      .outputMode("append")
      .option("checkpointLocation", settings.checkpointPaths("hashtag_trends"))
      .start()

    query.awaitTermination()
  }
}
