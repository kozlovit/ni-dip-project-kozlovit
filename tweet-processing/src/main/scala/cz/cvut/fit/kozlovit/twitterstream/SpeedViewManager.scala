package cz.cvut.fit.kozlovit.twitterstream

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.curator.framework.{CuratorFramework, CuratorFrameworkFactory}
import org.apache.curator.retry.ExponentialBackoffRetry

/** Speed view manager
 *
 *  A helper object for keeping synchronization between batch and speed layer by signaling
 *  which view tables are active( = should be queried to retrieve valid results) using Zookeeper.
 *
 */
object SpeedViewManager {
  val conf: Config = ConfigFactory.load()
  val settings: Settings = new Settings(conf)

  val retryPolicy = new ExponentialBackoffRetry(1000, 5)
  val nodePath: String = settings.zookeeper("active_view_path")

  /** Get id of active speed view table */
  def getActiveView: String = {
    val zkClient = getZookeeperClient
    Option(zkClient.checkExists().forPath(nodePath)) match {
      case None =>
        val currentView = "0"
        zkClient.create().forPath(nodePath, serialize(currentView))
        zkClient.close()
        currentView

      case Some(_) =>
        val rawViewValue = zkClient.getData.forPath(nodePath)
        val currentView = deserialize(rawViewValue)
        zkClient.close()
        currentView
    }
  }

  /** Change active view tables */
  def toggleView(): Unit = {
    val zkClient = getZookeeperClient

    val rawOldValue = zkClient.getData.forPath(nodePath)
    val oldValue = deserialize(rawOldValue)
    val newValue = if (oldValue == "0") "1" else "0"

    zkClient.setData().forPath(nodePath, serialize(newValue))
    zkClient.close()
  }

  /** Get Zookeeper Client Session */
  private def getZookeeperClient: CuratorFramework = {

    val zkClient = CuratorFrameworkFactory.newClient(settings.zookeeper("address"), retryPolicy)
    zkClient.start()
    zkClient.blockUntilConnected()
    zkClient
  }

  private def serialize(string: String): Array[Byte] = string.getBytes("UTF-8")

  private def deserialize(bytes: Array[Byte]) = new String(bytes, "UTF-8")

}