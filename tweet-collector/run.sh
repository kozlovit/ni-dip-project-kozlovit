#!/bin/sh

python -u tweet_collector.py \
  --consumer_key "$TWITTER_CONSUMER_KEY" \
  --consumer_secret "$TWITTER_CONSUMER_SECRET" \
  --access_token "$TWITTER_ACCESS_TOKEN" \
  --access_token_secret "$TWITTER_ACCESS_TOKEN_SECRET" \
  --bootstrap_servers "$KAFKA_ADDRESS_PRODUCER" \
  --kafka_topic "$KAFKA_TOPIC_TWEETS" \
  --schema_path "$TWEET_SCHEMA_PATH" 

