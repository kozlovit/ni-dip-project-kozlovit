import time
import logging
import click
import tweepy
import avro.schema
from kafka import KafkaProducer
from ssl import SSLError
from requests.exceptions import Timeout, ConnectionError
from urllib3.exceptions import ReadTimeoutError

from utils.utils import TimeMarker, ExponentialCounter, avro_encode


class TweetListener(tweepy.StreamListener):
    """
    Twitter stream listener using Twitter API.
    """

    def __init__(self):
        self._subscribers = []
        super().__init__()

    def attach(self, subscriber):
        self._subscribers.append(subscriber)

    def detach(self, subscriber):
        self._subscribers.remove(subscriber)

    def notify(self, message):
        for subscriber in self._subscribers:
            subscriber.update(message)

    def on_status(self, status):
        self.notify(status)


class KafkaTwitterProducer(KafkaProducer):
    """
    Kafka producer for incoming tweet messages.
    """

    def __init__(self, schema_path, kafka_topic, **configs):
        self.avro_schema = avro.schema.parse(open(schema_path).read())
        self.kafka_topic = kafka_topic
        super().__init__(**configs)

    def update(self, message):
        raw_bytes = avro_encode(
            {
                'userid': message.author.id,
                'username': message.author.name,
                'timestamp': int(message.timestamp_ms), 
                'tweetid': message.id,
                'tweet': message.text
            }, 
            self.avro_schema
        )
        self.send(self.kafka_topic, raw_bytes)


@click.command()
@click.option('--consumer_key', '-ck', required=True, help='Twitter API consumer key.')
@click.option('--consumer_secret', '-cs', required=True, help='Twitter API consumer secret.')
@click.option('--access_token', '-at', required=True, help='Twitter API access token.')
@click.option('--access_token_secret', '-ats', required=True, help='Twitter API access token secret.')
@click.option('--bootstrap_servers', '-bs', required=True,
              help='Comma-separated list of host and port pairs that are the addresses of the Kafka brokers.')
@click.option('--kafka_topic', '-kt', required=True, help='Kafka topic to write tweets into')
@click.option('--schema_path', '-sp', required=True, help='Path to the avro schema file')
def main(consumer_key, consumer_secret, access_token, access_token_secret, bootstrap_servers, kafka_topic, schema_path):
    counter = ExponentialCounter(60)
    timer = TimeMarker(3600)
    while True:
        try:
            # authorize
            auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
            auth.set_access_token(access_token, access_token_secret)

            kafka_producer = KafkaTwitterProducer(schema_path, kafka_topic, bootstrap_servers=bootstrap_servers)
            tweet_listener = TweetListener()
            tweet_listener.attach(kafka_producer)

            # initialize stream
            my_stream = tweepy.Stream(auth, listener=tweet_listener)
            my_stream.sample()
        except KeyboardInterrupt:
            break
        except (Timeout, SSLError, ReadTimeoutError, ConnectionError) as e:
            logging.warning(f"Network exception occurred. Type:{ e.__class__.__name__}, Exception message:{e}")
            if timer.is_expired():
                counter.reset()
            timer.reset()
            time.sleep(counter.counter())
            continue
        except Exception as e:
            logging.error(f"Unexpected exception. Type: {e.__class__.__name__}, Exception message:{e}")
            raise e


if __name__ == "__main__":
    main()
