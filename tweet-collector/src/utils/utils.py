import time
import random
import io
import avro.io


class ExponentialCounter:
    """
    A class to provide an exponential counter.
    """

    def __init__(self, max_counter: int):
        """
        Initialize an instance of ExponentialCounter.
        :param max_counter: The maximum base value.
        """
        self._base = 1
        self._max = max_counter

    def counter(self):
        """
        Increment the counter and return the current value.
        """
        value = self._base
        self._base = min(self._base * 2, self._max)
        return value

    def reset(self):
        """
        Reset the counter to 1.
        """
        self._base = 1
        
        
class TimeMarker:
    """
    A class to provide stopwatch functionality
    """

    def __init__(self, lifetime):
        """
        Initialize an instance of TimeMarker.
        :param lifetime: The maximum age of TimeMarker in seconds.
        """
        self._marker = time.time()
        self._lifetime = lifetime
        
    def is_expired(self):
        return self.timediff() > self._lefetime

    def timediff(self):
        """
        Get passed time since the the marker.
        """
        return time.time() - self._marker

    def reset(self):
        """
        Reset the marker.
        """
        self._marker = time.time()


def avro_encode(data, schema):
    """
    Encode message to avro format using specified schema.
    """
    writer = avro.io.DatumWriter(schema)
    bytes_writer = io.BytesIO()
    encoder = avro.io.BinaryEncoder(bytes_writer)
    writer.write(data, encoder)
    return bytes_writer.getvalue()
