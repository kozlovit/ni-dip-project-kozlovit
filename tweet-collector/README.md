# Tweet Collector 

A simple Python application that collects tweets through Twitter API and serves as a Kafka producer.

Building docker image : `docker build -t tweet_collector .`