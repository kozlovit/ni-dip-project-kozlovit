# Spark image 

Apache Spark 3.0.0. image including tweet processing code.

Run `get_dependencies.sh` script before building to download required dependencies.

Building docker image : `docker build -t spark3_twitter .`