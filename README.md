# Real-time Data Stream Processing System

This project is part of the Master's thesis at FIT, CTU in Prague. 
It aims to showcase a system design that tackles unique aspects of processing streaming data described in the thesis - it is highly available, horizontally scalable and allows low-latency data processing. This application focuses on the use case of social media streams and time series data.
![alt text](media/trending-example.gif)
![alt text](media/trendline-example.gif)

## Tweet stream processing

Twitter is an American microblogging and social networking service on which users post and interact with messages known as "tweets". It is estimated that around 6000 tweets are created every second. Twitter's streaming API provides free sample (1%) of these tweets, which makes it a perfect source of data for the reference application showcasing the benefits of the proposed system. The application will utilize both historical and real-time data, allowing users access to the relevant data on the fly based on the current context. This need becomes more and more important as real-time predictive analytics is becoming critical to modern enterprises.

In this reference application the tweet stream is used to identify and classify trending hashtags on Twitter, provide real-time changes in trends and fast access to the historical data for individual topics. The hashtag data is processed to provide almost real-time updates, showing evolving trends of hashtag occurrences in tweets. On top of that, the application provides rankings of top trendy hashtags for the past 10 minutes. These rankings could be thought as a changing real-time context to showcase fast retrieval of historical data for ranked hashtags.

## Design overview

The design is inspired by a well established Lambda architecture with some changes introduced to address its complexity drawbacks, utilize modern trends in public cloud computing and to best suit the domain of social media analytics.

![alt text](media/data-pipeline.png)

Tweets are collected using [Tweet Collector](./tweet-collector/README.md) and ingested into Apache Kafka topic. 

The batch layer allows to periodically read newly accumulated messages in Kafka topic and store them into a master dataset. The whole dataset is then processed using Apache Spark  in order to produce required views (Apache Cassandra tables).

To compensate for the delays caused by dataset processing, speed layers produces real-time views using Spark Structured Streaming.
  
[Dashboard](./dashboard/README.md) application is used to serve the data to the user by querying Cassandra tables (batch and real-time views) and combining results. 

The whole system is designed to run on Kubernetes cluster.

## Deployment overview

The sample application can be deployed on any Kubernetes cluster (v1.17), but requires adjusting some specifics accordingly (networking service and shared file system). 

One of the options is using [Amazon EKS](https://aws.amazon.com/eks) in combination with [Amazon EFS](https://aws.amazon.com/efs/) for share file system.
For a simple cluster deployment on EKS use [eksctl](https://eksctl.io/).  A good starting point for testing purposes is to run cluster of 5 `t3a.large` nodes, however the costs for the cluster are not covered by AWS Free Tier.

Running the sample application requires having [Twitter developer account](https://developer.twitter.com/en/apply-for-access). 

Components:
* Apache Kafka and Apache Cassandra installation scripts and configurations can be found in  `./deployment/kafka` and `./deployment/cassandra` respectively. More information about the charts available on [Bitnami github](https://github.com/bitnami/charts)
* Spark Operator installation script can be found in `./deployment/spark`. More information about Spark Operator available on [GoogleCloudPlatform github](https://github.com/GoogleCloudPlatform/spark-on-k8s-operator). Manifests describing Spark jobs can be found in `./deployment/spark`
* Tweet Collector manifest files is available in `./deployment/tweet-collector`
* Dashboard manifest files can be found in `./deployment/dashboard`
