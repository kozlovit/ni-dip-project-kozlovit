#!/bin/sh

if [ $# -lt 1 ]; then
  echo 1>&2 "$0: Specify installation config file"
  return
elif [ $# -gt 1 ]; then
  echo 1>&2 "$0: Too many arguments"
  return
fi

helm repo add bitnami https://charts.bitnami.com/bitnami

helm install kafka \
-f $1 bitnami/kafka