#!/bin/sh

#!/bin/sh

if [ $# -lt 1 ]; then
  echo 1>&2 "$0: Specify installation config file"
  return
elif [ $# -gt 1 ]; then
  echo 1>&2 "$0: Too many arguments"
  return
fi

helm repo add bitnami https://charts.bitnami.com/bitnami

helm install cassandra \
    -f $1 \
    --set dbUser.user=$CASSANDRA_USER,dbUser.password=$CASSANDRA_PASSWORD \
    bitnami/cassandra