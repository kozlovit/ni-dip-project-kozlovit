"""
Queries for setting up Cassandra tables.
"""

create_keyspace_dev = """
CREATE KEYSPACE twitter
  WITH REPLICATION = { 
   'class' : 'SimpleStrategy', 
   'replication_factor' : 1 
  };
"""

create_keyspace_test = """
CREATE KEYSPACE twitter
  WITH REPLICATION = { 
   'class' : 'SimpleStrategy', 
   'replication_factor' : 3 
  };
"""

create_batch_views = """
CREATE TABLE twitter.batch_view_0(
  hashtag text,
  date date,
  hour int,
  min_5 int,
  occurrences int,
  PRIMARY KEY ((hashtag), date, hour, min_5)) 
WITH CLUSTERING ORDER BY (date DESC, hour DESC, min_5 DESC);

CREATE TABLE twitter.batch_view_1(
  hashtag text,
  date date,
  hour int,
  min_5 int,
  occurrences int,
  PRIMARY KEY ((hashtag), date, hour, min_5)) 
WITH CLUSTERING ORDER BY (date DESC, hour DESC, min_5 DESC);
"""

create_speed_views = """
CREATE TABLE twitter.speed_view_0(
  hashtag text,
  date date,
  hour int,
  min_5 int,
  occurrences counter,
  PRIMARY KEY ((hashtag), date, hour, min_5)) 
WITH CLUSTERING ORDER BY (date DESC, hour DESC, min_5 DESC);

CREATE TABLE twitter.speed_view_1(
  hashtag text,
  date date,
  hour int,
  min_5 int,
  occurrences counter,
  PRIMARY KEY ((hashtag), date, hour, min_5)) 
WITH CLUSTERING ORDER BY (date DESC, hour DESC, min_5 DESC);

"""