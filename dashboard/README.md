# Dashboard application

Simple Flask web application providing live trend charts based on data from Cassandra/Kafka through WebSocket. 

Building docker image : `docker build -t hashtag-dashboard .`