$(document).ready(function(){
    // Connect to the socket server
    var socket = io.connect("http://" + document.domain + ":" + location.port + "/trends");
    var numbers_received = [];

    socket.on("update", function (msg) {
        streamTrends(msg.hashtags, msg.values, msg.timestamp)
    });
    // Prepare plots
    plotRankings();
    plotTrendline();
});

function formatTime(s) {
    var dtFormat = new Intl.DateTimeFormat("en-GB", {
        timeStyle: "medium",
        timeZone: "UTC"
    });
  return dtFormat.format(new Date(s * 1e3));
}

function twitter_link(hashtag) {
    return "<a href=\"https://twitter.com/hashtag/" + String(hashtag).substring(1) + "\">" + hashtag + "</a>";
}

// Updates rankings chart after receiving new data
function streamTrends(x, y, timestamp){
    var plotDiv = document.getElementById("rankings");

    x.forEach((tag, i) => {
        x[i] = twitter_link(tag);
    });
    var data_update = {x: [y], y: [x]}
    var layout_update = {
        title: "Trendy hashtags in last 10 minutes (Updated: " + formatTime(timestamp) + ")"
    };

    Plotly.update(plotDiv, data_update, layout_update)
};

// Updates hashtag occurrences trendline chart after receiving data
function updateTrendline(hashtag, data) {
    var plotDiv = document.getElementById("trendline");
    var data_update = {x: [data.dates], y: [data.values]}
    var layout_update = {
        title: "Occurrences of hashtag " + twitter_link(hashtag) + " on Twitter in time"
    };
    Plotly.update(plotDiv, data_update, layout_update)
}

// Requests data for hashtag occurrences trendline chart
function getTrendline(hashtag){
    var url = "http://" + document.domain + ":" + location.port + "/hashtag?value=" + encodeURIComponent(hashtag);
    (async()=>
        updateTrendline(hashtag, (await (await fetch(url)).json()))
    )();
}

// Plots rankings chart
function plotRankings(){
    var plotDiv = document.getElementById("rankings");

    var traces = [{
        type: "bar",
        orientation: "h",
        hoverinfo: "x",
            transforms: [{
                type: "sort",
                target: "x",
                order: "ascending"
            }]
        }];

    var layout = {
        title: {
            font: { size: 28 }
        },
        autosize: true,
      yaxis: {
        showticklabels: true,
        tickfont: { size: 22 }
      },
        font: {size: 18},
          margin: {
            l: 300,
            r: 250,
            b: 100,
            t: 150,
            pad: 4
          },
    };

    var additional_params = {
        responsive: true
    };

    Plotly.plot(plotDiv, traces, layout, additional_params);

    plotDiv.on("plotly_click", function(data) {
        var patt = /(#.+)<\/a>/;
        var clickedTag = data.points[0].label.match(patt)[1];
        getTrendline(clickedTag);
    });

    dragLayer = document.getElementsByClassName("nsewdrag")[0];
    plotDiv.on("plotly_hover", function(data) {
        dragLayer.style.cursor = "pointer"
    });
};

// Plots hashtag occurrences trendline chart
function plotTrendline(){
    var plotDiv = document.getElementById("trendline");

    var traces = [{
        type: "line"
    }];

    var layout = {
        autosize: true,
        title: {
            font: { size: 28 }
        },
        xaxis: {
            type: "date",
            range: [new Date() - 1000 * 60 * 60 * 24, new Date()],
            rangeslider: {}
        },
        font: { size: 18 },
        margin: {
            l: 250,
            r: 100,
            b: 100,
            t: 150,
            pad: 4
        },
    };

    var additional_params = {
        responsive: true
    };

    Plotly.plot(plotDiv, traces, layout, additional_params);
};



