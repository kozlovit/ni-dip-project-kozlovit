from configparser import ConfigParser

from utils.utils import EnvInterpolation

settings = ConfigParser(interpolation=EnvInterpolation())
settings.read("config.ini")
env = "test"