from datetime import datetime, timezone
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider

from settings import settings, env


def hashtag_lookup_template(table):
    query = """
        SELECT date, hour, min_5, occurrences
        FROM {}
        WHERE hashtag=?;
        """
    return query.format(table)


class ServingLayer:
    """
    Serving layer providing complete result of hashtag occurrences on Twitter in time
    by combining results from batch and speed views.
    """

    def __init__(self, cass_session):
        self._cassandra = cass_session
        self._statements = None
        self._active_batch_view = None
        self._active_speed_view = None

    def prepare_statements(self):
        tables = [settings.get(env, "cassandra_batch_view_0"),
                  settings.get(env, "cassandra_batch_view_1"),
                  settings.get(env, "cassandra_speed_view_0"),
                  settings.get(env, "cassandra_speed_view_1")]

        self._statements = {table: self._cassandra.prepare_statement(hashtag_lookup_template(table))
                            for table in tables}

    def set_active_view(self, view):
        self._active_batch_view = settings.get(env, "cassandra_batch_view_" + view)
        self._active_speed_view = settings.get(env, "cassandra_speed_view_" + view)

    def lookup_hashtag(self, name):
        batch_results = self._cassandra.query(self._statements[self._active_batch_view], [name])
        speed_results = self._cassandra.query(self._statements[self._active_speed_view], [name])

        dates, values = [], []
        for result_set in [speed_results, batch_results]:
            for row in result_set:
                dates.append(
                    datetime.fromtimestamp(
                        (row.date.seconds + row.hour * 3600 + row.min_5 * 60)).strftime("%Y-%m-%d %H:%M:%S"))
                values.append(row.occurrences)

        result = {
            "dates": dates,
            "values": values
        }

        return result


class CassandraConnector:
    """
    Connector serving to ease Cassandra statement preparation and querying.
    """

    def __init__(self):
        self._cluster = None
        self._session = None

    def connect(self):
        auth_provider = PlainTextAuthProvider(username=settings.get(env, "cassandra_username"),
                                              password=settings.get(env, "cassandra_password"))
        self._cluster = Cluster([settings.get(env, "cassandra_host")], auth_provider=auth_provider)
        self._session = self._cluster.connect()

    def disconnect(self):
        self._cluster.shutdown()

    def prepare_statement(self, statement):
        return self._session.prepare(statement)

    def query(self, statement, params):
        return self._session.execute(statement, params)
