from settings import settings, env
from serving_layer import CassandraConnector, ServingLayer
from utils.utils import avro_decode

import os
from kazoo.client import KazooClient
from flask_socketio import SocketIO
from flask import Flask, render_template, request, Response
from threading import Event
from kafka import KafkaConsumer
import avro.schema
import avro.io
import json

user_count = 0
flag = Event()
cassandra = CassandraConnector()
serving_layer = ServingLayer(cassandra)
zookeeper = KazooClient(hosts=settings.get(env, "zookeeper_host"))

app = Flask(__name__)
app.secret_key = os.urandom(42)
app.config["DEBUG"] = True

socketio = SocketIO(app, async_mode="eventlet", logger=True, engineio_logger=True)


def stream_kafka():
    """
    Transmits Twitter trends from Kafka to clients through WebSocket.
    """
    kafka_consumer = KafkaConsumer(settings.get(env, "kafka_topic_trends"),
                                   bootstrap_servers=settings.get(env, "kafka_bootstrap_servers"),
                                   consumer_timeout_ms=100)

    schema_path = settings.get(env, "trends_schema_path")
    schema = avro.schema.parse(open(schema_path).read())
    while flag.is_set():
        for msg in kafka_consumer:
            rankings_data = avro_decode(msg, schema)
            rankings = {
                "timestamp": rankings_data["timestamp"],
                "hashtags": [],
                "values": []
            }
            for row in rankings_data["hashtag_list"]:
                rankings["hashtags"].append(row["hashtag"])
                rankings["values"].append(row["occurrences"])
            socketio.emit("update", rankings, namespace="/trends")
        if not flag.is_set():
            break
        socketio.sleep(1)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/hashtag")
def get_hashtag():
    """
    Endpoint for retrieving historical data for hashtag occurrences in Tweets.
    """
    hashtag = request.args.get("value")
    response_data = serving_layer.lookup_hashtag(hashtag)

    json_response = json.dumps(response_data)
    response = Response(json.dumps(response_data), content_type="application/json; charset=utf-8")
    response.headers.add("content-length", len(json_response))
    response.status_code = 200
    return response


@socketio.on("connect", namespace="/trends")
def on_connect():
    global user_count
    user_count += 1

    if not flag.is_set():
        flag.set()
        cassandra.connect()
        serving_layer.prepare_statements()
        zookeeper.start()
        socketio.start_background_task(stream_kafka)


@socketio.on("disconnect", namespace="/trends")
def on_disconnect():
    global user_count

    user_count -= 1
    if user_count == 0:
        flag.clear()
        cassandra.disconnect()
        zookeeper.stop()


@zookeeper.DataWatch("/active_view")
def set_speed_view(data, stat):
    """
    Sets correct speed view after receiving signal from ZooKeeper
    """
    serving_layer.set_active_view(data.decode("utf-8"))


if __name__ == '__main__':
    socketio.run(app, host="0.0.0.0")


